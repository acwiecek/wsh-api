from faker import Faker
import endpoint
import requests
fake = Faker()

class Account:
    def __init__(self):
        self.name = fake.uuid4()

    def create(self):
        body = {"name": self.name}
        create_account_response = requests.put(endpoint.accounts_create, json=body)
        assert create_account_response.status_code == 201

    def delete(self):
        account_params = {'account': self.name}
        delete_response = requests.delete(endpoint.accounts_delete, params=account_params)
        assert delete_response.status_code == 200

    def pay(self, amount):
        account_params = {'account': self.name}
        body = {'amount' : amount}
        pay_response = requests.post(endpoint.accounts_pay, json=body, params=account_params)
        assert pay_response.status_code == 200

    def withdraw(self, amount):
        account_params = {'account': self.name}
        body = {'amount': amount}
        withdraw_response = requests.post(endpoint.accounts_withdraw, json=body, params=account_params)
        assert withdraw_response.status_code == 200

    def get_balance(self):
        list_params = {'account': self.name}
        filter_balanse = requests.get(endpoint.accounts, params=list_params)
        return filter_balanse.json()['accounts'][0]['balance']['accountBalance']



if __name__ == '__main__':#ctrl m
    account = Account()
    print(account.name)
    account.create()
    account.delete()



