import requests
import  endpoint
from faker import Faker

fake = Faker()

def test_get_accounts_list():
    base_url = 'http://18.184.234.77:8080'
    endpoint ='/accounts'
    response = requests.get(base_url+endpoint)
    assert response.status_code == 200
    print(response.status_code)
    response_dict = response.json()
    print(response_dict['accounts'])
    accounts_list = response_dict['accounts']
    name_list = [a['name'] for a in accounts_list]
    print(name_list)
    assert 'adrian11' in name_list

def test_create_account():
    expected_name = _create_account()
    list_params = {'account' :expected_name}
    filter_list_response = requests.get(endpoint.accounts , params=list_params)
    assert filter_list_response.status_code == 200
    assert  expected_name in filter_list_response.text

def _create_account(): #metoda pomocnicza z podkreślinikeim
    random_name = fake.uuid4()
    body = {
        "name": random_name
    }
    create_account_response = requests.put(endpoint.accounts_create, json=body)
    assert create_account_response.status_code == 201
    return  random_name


