import pytest
import requests
import  endpoint
from faker import Faker

from models.Account import Account

fake = Faker()

def test_get_accounts_list(new_account):
    response = requests.get(endpoint.accounts)
    assert response.status_code == 200
    print(response.status_code)
    response_dict = response.json()
    print(response_dict['accounts'])
    accounts_list = response_dict['accounts']
    name_list = [a['name'] for a in accounts_list]
    print(name_list)
    assert new_account.name in name_list

def test_create_account(new_account):
    list_params = {'account': new_account.name}
    filter_list_response = requests.get(endpoint.accounts , params=list_params)
    assert filter_list_response.status_code == 200
    assert  new_account.name in filter_list_response.text


def test_delete_account(new_account):
    new_account.delete()
    account_params = {'account': new_account.name}
    filter_list_response = requests.get(endpoint.accounts, params=account_params)
    assert filter_list_response.status_code == 404

@pytest.fixture #Alt lewy +Enter żeby zaimportowało
def new_account(): #metoda pomocnicza z podkreślinikeim
    account = Account()# Ale+entrer i imoort
    account.create()
    return  account

def test_balance(new_account):
    assert new_account.get_balance() == 1000


def test_1(new_account):
    new_account.pay(200)
    assert new_account.get_balance() == 1200
    new_account.withdraw(133)
    assert new_account.get_balance() == 1067
