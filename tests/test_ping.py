import requests
import  endpoint


def test_get_ping():
    ping_response = requests.get(endpoint.ping)
    assert ping_response.status_code == 200
    print(ping_response.status_code)
    response = ping_response.text
    print(response)
    assert response == 'pong'

def test_get_ping_as_json():
    headers_dict ={
        'Accept' : 'application/json'
    }

    ping_response = requests.get(endpoint.ping, headers=headers_dict)
    assert ping_response.status_code == 200
    print(ping_response.status_code)
    response_dict = ping_response.json()
    assert response_dict['reply'] == 'pong!'